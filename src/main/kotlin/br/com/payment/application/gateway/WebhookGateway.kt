package br.com.payment.application.gateway

interface WebhookGateway {

    fun paymentConfirmed(numberOrder: String)

    fun paymentRefused(numberOrder: String)

}