package br.com.payment.application.gateway.impl

import br.com.payment.application.gateway.WebhookGateway
import br.com.payment.core.useCase.IOrderUseCase
import br.com.payment.infra.api.enums.OrderStatus
import org.springframework.stereotype.Service

@Service
class WebhookGatewayImpl(
        private val useCaseOrder: IOrderUseCase
): WebhookGateway {

    override fun paymentConfirmed(numberOrder: String) {
        useCaseOrder.updateStatus(numberOrder, OrderStatus.PAYMENT_CONFIRMED)
        useCaseOrder.updateStatus(numberOrder, OrderStatus.IN_PREPARATION)
    }

    override fun paymentRefused(numberOrder: String) {
        useCaseOrder.updateStatus(numberOrder, OrderStatus.PAYMENT_REFUSED)
    }


}