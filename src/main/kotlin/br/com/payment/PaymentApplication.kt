package br.com.payment

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients
class FastfoodApplication

fun main(args: Array<String>) {
	runApplication<FastfoodApplication>(*args)
}
