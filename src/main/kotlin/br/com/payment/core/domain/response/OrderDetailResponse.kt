package br.com.payment.core.domain.response

import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.enums.TypeDelivery
import java.time.LocalDateTime

data class OrderDetailResponse(
        val numberOrder: String,
        val client: ClientResponse? = null,
        val total: Double,
        val status: OrderStatus,
        val items: MutableList<OrderItemResponse>,
        val typeDelivery: TypeDelivery,
        val createAt: LocalDateTime
)
