package br.com.payment.core.domain.response

import br.com.payment.infra.api.enums.OrderStatus

data class CheckoutResponse(
        val numberCheckout: String,
        val status: OrderStatus
)
