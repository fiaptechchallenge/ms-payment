package br.com.payment.core.domain

import br.com.payment.infra.api.OrderApi
import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.api.response.OrderDetailResponse
import br.com.payment.infra.enums.TypePayment
import org.bson.types.ObjectId
import java.time.LocalDateTime

data class Checkout(
        val id: ObjectId? = null,
        val order: OrderDetailResponse,
        val numberCheckout: String,
        val typePayment: TypePayment,
        val card: Card,
        val value: Double,
        val status: OrderStatus,
        val createAt: LocalDateTime

)
