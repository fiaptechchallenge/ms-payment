package br.com.payment.core.domain.response

data class ClientResponse(
        val name: String? = null,
        val email: String? = null,
        val cpf: String? = null
)
