package br.com.payment.core.domain

import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.enums.TypeDelivery
import org.bson.types.ObjectId
import java.time.LocalDateTime

data class Order (
        val id: ObjectId? = null,
        var numberOrder: String,
        val client: Client? = null,
        val deliveryAddress : Address? = null,
        val items: MutableList<OrderItem>,
        val total: Double,
        var status: OrderStatus,
        val typeDelivery: TypeDelivery,
        val createAt: LocalDateTime
)