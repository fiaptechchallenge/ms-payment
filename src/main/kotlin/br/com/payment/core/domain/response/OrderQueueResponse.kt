package br.com.payment.core.domain.response

import br.com.payment.infra.api.response.OrderResponse

data class OrderQueueResponse(

        var done: MutableList<OrderResponse>? = null,
        var inPreparation: MutableList<OrderResponse>? = null,
        var receveid: MutableList<OrderResponse>? = null,

        )
