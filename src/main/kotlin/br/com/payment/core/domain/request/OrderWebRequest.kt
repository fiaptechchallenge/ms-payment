package br.com.payment.core.domain.request

import br.com.payment.infra.enums.TypeDelivery
import br.com.payment.core.domain.Address

data class OrderWebRequest(
        val emailClient: String? = null,
        val deliveryAddress : Address? = null,
        val items: MutableList<OrderItemRequest>,
        val typeDelivery: TypeDelivery
)



