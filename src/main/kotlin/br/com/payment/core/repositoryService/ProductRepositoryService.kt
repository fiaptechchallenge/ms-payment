package br.com.payment.core.repositoryService

import br.com.payment.infra.enums.ProductCategory
import br.com.payment.core.domain.Product

interface ProductRepositoryService {

    fun create(product: Product): Product

    fun findByCategory(category: ProductCategory): MutableList<Product>

    fun findByAll(): MutableList<Product>

    fun findByCode(code: String): Product?

    fun delete(product: Product)

}