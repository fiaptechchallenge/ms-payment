package br.com.payment.core.repositoryService

import br.com.payment.core.domain.Client

interface ClientRepositoryService {

    fun create(client: Client): Client
    fun findByDoc(cpf: String): Client?
    fun findByEmail(email: String): Client?
    fun list(): MutableList<Client>
}