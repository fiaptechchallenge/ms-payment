package br.com.payment.core.useCase

import br.com.payment.core.domain.Checkout
import br.com.payment.core.domain.request.CheckoutRequest

interface IUseCaseCheckout {

    fun checkout(checkoutRequest: CheckoutRequest): Checkout

    fun findByNumberCheckout(numberCheckout: String): Checkout
}