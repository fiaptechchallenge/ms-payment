package br.com.payment.core.useCase

import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.api.response.OrderDetailResponse


interface IOrderUseCase {

    fun findByNumberOrder(numberOrder: String): OrderDetailResponse

    fun updateStatus(numberOrder: String, status: OrderStatus): OrderDetailResponse

}