package br.com.payment.core.useCase.impl

import br.com.payment.core.domain.Client
import br.com.payment.core.domain.Order
import br.com.payment.core.domain.OrderItem
import br.com.payment.core.domain.exception.FastFoodException
import br.com.payment.core.domain.exception.NotFoundException
import br.com.payment.core.domain.request.ClientOrderRequest
import br.com.payment.core.domain.request.OrderStoreRequest
import br.com.payment.core.domain.response.OrderQueueResponse
import br.com.payment.core.useCase.IOrderUseCase
import br.com.payment.infra.api.OrderApi
import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.api.response.OrderDetailResponse
import br.com.payment.infra.extensions.toResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.time.LocalDateTime.now
import java.util.*

@Service
class OrderUseCase(
        private val orderApi: OrderApi
): IOrderUseCase {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun findByNumberOrder(numberOrder: String): OrderDetailResponse {
        logger.info("{} - Find Order by number order: {}", this::class.java, numberOrder)
        return orderApi.findByNumberOrder(numberOrder)
            .body
            ?: throw FastFoodException(message = "Order not found", code = HttpStatus.BAD_REQUEST)
    }

    override fun updateStatus(numberOrder: String, status: OrderStatus): OrderDetailResponse {
        logger.info("{} - Update order by numberOrder: {} - status: {}", this::class.java, numberOrder, status)
        val order = findByNumberOrder(numberOrder)

        if(status == order.status) {
            throw FastFoodException(message = "The order is already in requested status ", code = HttpStatus.BAD_REQUEST)
        }
        orderApi.update(numberOrder, status)
        return order
    }
}