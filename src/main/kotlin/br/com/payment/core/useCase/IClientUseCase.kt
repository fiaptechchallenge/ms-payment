package br.com.payment.core.useCase

import br.com.payment.core.domain.Client
import br.com.payment.core.domain.request.ClientOrderRequest

interface IClientUseCase {

    fun findByDoc(cpf: String): Client
    fun create(client: Client): Client
    fun findByEmail(email: String): Client?
    fun findOrCreate(clientOrderRequest: ClientOrderRequest?): Client

}