package br.com.payment.core.useCase.impl

import br.com.payment.core.domain.Client
import br.com.payment.core.domain.exception.NotFoundException
import br.com.payment.core.domain.request.ClientOrderRequest
import br.com.payment.infra.extensions.toClient
import br.com.payment.core.repositoryService.ClientRepositoryService
import br.com.payment.core.useCase.IClientUseCase
import org.springframework.stereotype.Service

@Service
class ClientUseCase(
        private val repository: ClientRepositoryService
): IClientUseCase {

    override fun findByDoc(cpf: String): Client {
        return repository.findByDoc(cpf)
                ?: throw NotFoundException("Client not Found")
    }

    override fun create(client: Client): Client {
        return repository.create(client)
    }

    override fun findByEmail(email: String): Client? {
        return repository.findByEmail(email)
    }

    override fun findOrCreate(clientOrderRequest: ClientOrderRequest?): Client {
        return clientOrderRequest
                ?.let { findByEmail(it.email!!) }
                ?: create(clientOrderRequest?.toClient()!!)

    }
}