package br.com.payment.infra.api.enums

enum class OrderStatus {

    NEW,
    AWAITING_PAYMENT,
    IN_PREPARATION,
    PAYMENT_CONFIRMED,
    PAYMENT_REFUSED,
    FINISHED,
    DONE,
    RECEIVED
}