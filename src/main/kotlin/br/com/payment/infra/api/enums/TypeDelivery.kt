package br.com.payment.infra.api.enums

enum class TypeDelivery {

    FOR_TRAVEL,
    EAT_HERE

}