package br.com.payment.infra.api.response

data class ClientResponse(
    val name: String? = null,
    val email: String? = null,
    val cpf: String? = null
)
