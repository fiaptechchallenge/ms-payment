package br.com.payment.infra.api

import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.api.response.OrderDetailResponse
import br.com.payment.infra.api.response.OrderResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping

@FeignClient(value = "ms-order", url = "\${feign-api.ms-order.uri}")
interface OrderApi {

    @GetMapping("/order/numberOrder/{numberOrder}")
    fun findByNumberOrder(@PathVariable numberOrder: String): ResponseEntity<OrderDetailResponse>

    @PutMapping("/order/{numberOrder}/status/{status}")
    fun update(@PathVariable numberOrder: String, status: OrderStatus): ResponseEntity<OrderResponse>

}