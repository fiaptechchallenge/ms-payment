package br.com.payment.infra.api.response

import br.com.payment.infra.api.enums.OrderStatus
import br.com.payment.infra.api.enums.TypeDelivery
import java.time.LocalDateTime

data class OrderDetailResponse(
    val numberOrder: String,
    val client: ClientResponse? = null,
    val total: Double,
    val status: OrderStatus,
    val items: MutableList<OrderItemResponse>,
    val typeDelivery: TypeDelivery,
    val createAt: LocalDateTime
)
