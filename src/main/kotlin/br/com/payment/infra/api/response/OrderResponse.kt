package br.com.payment.infra.api.response

import br.com.payment.infra.api.enums.OrderStatus
import java.time.LocalDateTime

data class OrderResponse(
    val numberOrder: String,
    val total: Double,
    val status: OrderStatus,
    val dateCreate: LocalDateTime
)
