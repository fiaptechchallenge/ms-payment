package br.com.payment.infra.enums

enum class ProductCategory {
    SNACK,
    DRINK,
    ACCOMPANIMENT,
    COMBO
}