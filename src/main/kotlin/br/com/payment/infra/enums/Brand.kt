package br.com.payment.infra.enums

enum class Brand {

    VISA,
    MASTER,
    ELO
}