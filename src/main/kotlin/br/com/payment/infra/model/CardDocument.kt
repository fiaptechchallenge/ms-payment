package br.com.payment.infra.model

import br.com.payment.infra.enums.Brand

data class CardDocument(
        val nameCard: String,
        val number: String,
        val dateValidate: String,
        val code: Number,
        val brand: Brand,
        val document: String
)
