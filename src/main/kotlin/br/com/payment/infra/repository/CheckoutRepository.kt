package br.com.payment.infra.repository

import br.com.payment.core.domain.Checkout
import br.com.payment.infra.extensions.toDocument
import br.com.payment.infra.extensions.toDomain
import br.com.payment.core.repositoryService.CheckoutRepositoryService
import br.com.payment.infra.port.CheckoutMongoDBPort
import org.springframework.stereotype.Repository

@Repository
class CheckoutRepository(
        private val checkoutMongoDBPort: CheckoutMongoDBPort
): CheckoutRepositoryService {


    override fun create(checkout: Checkout): Checkout {
        return checkoutMongoDBPort.save(checkout.toDocument()).toDomain()
    }

    override fun findByNumerCheckout(numberCheckout: String): Checkout? {
        return checkoutMongoDBPort.findByNumberCheckout(numberCheckout)?.toDomain()
    }

    override fun existNumbercheckout(number: String): Boolean {
        return checkoutMongoDBPort.existsByOrderNumberOrder(number)
    }
}