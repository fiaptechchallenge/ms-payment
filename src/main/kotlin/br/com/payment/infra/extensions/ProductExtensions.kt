package br.com.payment.infra.extensions

import br.com.payment.infra.model.ProductDocument
import br.com.payment.core.domain.Product
import br.com.payment.core.domain.request.ProductRequest
import br.com.payment.core.domain.response.ProductResponse

fun ProductDocument.toDomain(): Product =
        Product(
                id = id,
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category,
                dateValidate = dateValidate
        )

fun Product.toDocument(): ProductDocument =
        ProductDocument(
                id =  id,
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category,
                dateValidate = dateValidate
        )

fun ProductRequest.toDomain(): Product =
        Product(
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category,
                dateValidate = dateValidate
        )

fun Product.toResponse(): ProductResponse =
        ProductResponse(
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category,
                dateValidate = dateValidate
        )