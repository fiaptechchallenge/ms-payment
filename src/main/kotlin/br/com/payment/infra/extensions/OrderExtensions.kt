package br.com.payment.infra.extensions

import br.com.payment.infra.model.OrderDocument
import br.com.payment.infra.model.OrderItemDocument
import br.com.payment.core.domain.Order
import br.com.payment.core.domain.OrderItem
import br.com.payment.core.domain.response.OrderDetailResponse
import br.com.payment.core.domain.response.OrderItemResponse
import br.com.payment.infra.api.response.OrderResponse

fun Order.toDocument(): OrderDocument =
        OrderDocument(
                id = id,
                numberOrder = numberOrder,
                client = client?.toDocument(),
                deliveryAddress = deliveryAddress?.toDocument(),
                items = items.map { it.toDocument() }.toMutableList(),
                total = total,
                status = status,
                typeDelivery = typeDelivery,
                createAt = createAt
        )

fun OrderDocument.toDomain(): Order =
        Order(
                id = id,
                numberOrder = numberOrder,
                client = client?.toDomain(),
                deliveryAddress = deliveryAddress?.toDomain(),
                items = items.map { it.toDomain() }.toMutableList(),
                total = total,
                status = status,
                typeDelivery = typeDelivery,
                createAt = createAt
        )

fun OrderItem.toDocument(): OrderItemDocument =
        OrderItemDocument(
                name = name,
                codeProduct = codeProduct,
                quantity = quantity,
                price = price
        )

fun OrderItemDocument.toDomain(): OrderItem =
        OrderItem(
                name = name,
                codeProduct = codeProduct,
                quantity = quantity,
                price = price
        )

fun Order.toResponse(): OrderResponse =
        OrderResponse(
                numberOrder = numberOrder,
                total = total,
                status = status,
                dateCreate = createAt
        )

fun Order.toOrderDetailResponse(): OrderDetailResponse =
        OrderDetailResponse(
                numberOrder = numberOrder,
                client = client?.toResponse(),
                total = total,
                status = status,
                items = items.map { it.toOrderItemResponse() }.toMutableList(),
                typeDelivery = typeDelivery,
                createAt = createAt
        )

fun OrderItem.toOrderItemResponse(): OrderItemResponse =
        OrderItemResponse(
                name = name,
                codeProduct = codeProduct,
                quantity = quantity,
                price = price
        )

