package br.com.payment.infra.extensions

import br.com.payment.infra.model.AddressDocument
import br.com.payment.core.domain.Address

fun Address.toDocument(): AddressDocument =
        AddressDocument(
                street = street,
                number = number,
                complement = complement,
                neighborhood = neighborhood,
                city = city,
                zipCode = zipCode
        )

fun AddressDocument.toDomain(): Address =
        Address(
                street = street,
                number = number,
                complement = complement,
                neighborhood = neighborhood,
                city = city,
                zipCode = zipCode
        )