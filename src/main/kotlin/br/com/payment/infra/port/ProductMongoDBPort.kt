package br.com.payment.infra.port

import br.com.payment.infra.enums.ProductCategory
import br.com.payment.infra.model.ProductDocument
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface ProductMongoDBPort: MongoRepository<ProductDocument, ObjectId> {

    fun findByCode(code: String): ProductDocument?

    fun findByCategory(category: ProductCategory): MutableList<ProductDocument>?
}